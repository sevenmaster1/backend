from json import dumps

from flask import Blueprint, send_file, Response

from FileManager import FileManager

raw = Blueprint("raw", __name__)


@raw.route("/", methods=["GET"])
def get_raw():
    return Response(dumps(FileManager.instance.get_json(), ensure_ascii=False), mimetype="application/json")
