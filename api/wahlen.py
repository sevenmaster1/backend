from flask import Blueprint, Response, request, abort

from CryptoManager import CryptoManager
from FileManager import FileManager
from json import dumps, loads

wahlen = Blueprint("wahl", __name__)


@wahlen.route("/", methods=["GET"])
def get_wahlen():
    state = FileManager.instance.get_state()
    ps = list(filter(lambda key: state[key]["type"] == "wahl", state))

    wahlen_dict = {}
    for key in ps:
        wahlen_dict[key] = state[key]
    print(wahlen_dict)

    return Response(dumps(wahlen_dict, ensure_ascii=False), mimetype="application/json")


@wahlen.route("/<path:wahl_id>", methods=["GET"])
def get_wahl(wahl_id):
    wahl = None
    for uuid, obj in FileManager.instance.get_state().items():
        if obj["type"] == wahl_id:
            wahl = obj

    return Response(dumps(wahl, ensure_ascii=False), mimetype="application/json")


@wahlen.route("/<path:wahl_id>", methods=["PUT"])
def update_wahl(wahl_id):
    json = FileManager.instance.get_json()

    data = CryptoManager.instance.validate_json_message(loads(request.data))
    print(data)
    if not data:
        abort(403)

    json.append({
        "UUID": wahl_id,
        "changes": data
    })
    FileManager.instance.set_json(json)

    return Response()
