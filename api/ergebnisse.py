from json import dumps

from flask import Blueprint, Response

from FileManager import FileManager

ergebnisse = Blueprint("ergebnisse", __name__)


@ergebnisse.route("/erststimmen", methods=["GET"])
def get_kandidat():
    state = FileManager.instance.get_state()
    wahlen = list(filter(lambda key: state[key]["type"] == "wahl", state))

    ges = 0
    kandidaten_dict = {}
    for key in wahlen:
        wahl = state[key]
        for partei, stimmen in wahl["erststimmen"]["stimmen"].items():
            if partei not in kandidaten_dict:
                kandidaten_dict[partei] = 0
            kandidaten_dict[partei] += int(stimmen)
            ges += int(stimmen)
    for key in kandidaten_dict:
        kandidaten_dict[key] /= ges
        kandidaten_dict[key] *= 100

    return Response(dumps(kandidaten_dict, ensure_ascii=False), mimetype="application/json")


@ergebnisse.route("/zweitstimmen", methods=["GET"])
def get_zweitstimmen():
    state = FileManager.instance.get_state()
    wahlen = list(filter(lambda key: state[key]["type"] == "wahl", state))

    ges = 0
    parteien_dict = {}
    for key in wahlen:
        wahl = state[key]
        for partei, stimmen in wahl["zweitstimmen"]["stimmen"].items():
            if partei not in parteien_dict:
                parteien_dict[partei] = 0
            parteien_dict[partei] += int(stimmen)
            ges += int(stimmen)
    for key in parteien_dict:
        parteien_dict[key] /= ges
        parteien_dict[key] *= 100

    return Response(dumps(parteien_dict, ensure_ascii=False), mimetype="application/json")
